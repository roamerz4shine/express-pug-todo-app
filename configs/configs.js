var dbName = () => {
  return 'project_todo';
}

var dbUrl = () => {
  return 'mongodb://localhost:27017/' + dbName();
}

module.exports = {
  dbName,
  dbUrl
}
