var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var todoSchema = new Schema({
  date: Date,
  task: String,
  done: Boolean
});

module.exports = mongoose.model('todo', todoSchema, 'testCollection');
