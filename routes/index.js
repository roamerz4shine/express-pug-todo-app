var express = require('express');
var router = express.Router();
var libs = require('./libs');
var configs = require('../configs/configs');
var todo = require('../models/todo');

/* GET home page. */
router.get('/', (req, res, next) => {
  todo.find({}, (err, docs) => {
    console.log(docs);
    res.render('index', {todos: docs});
  });
});

router.post('/task', (req, res) => {
  var todoItem = new todo();
  todoItem.date = new Date(req.body.date);
  todoItem.task = req.body.task;
  todoItem.done = false;
  todoItem.save((err) => {
    if (err) console.log(err);
    res.redirect('/');
  });
});

router.post('/task-done', (req, res) => {
  var checked = req.body.checked;
  var objectId = [];

  if (libs.isArray(checked)) {
    for (var i = 0 ; i < checked.length ; i++) {
      objectId.push(checked[i]);
    }
  } else {
    objectId.push(checked);
  }

  todo.update({'_id': {$in: objectId}}, {$set: {'done': true}}, {multi: true}, (err) => {
                if (err) console.log(err);
                res.redirect('/');
              });
});

module.exports = router;
