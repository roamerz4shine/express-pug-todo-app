var isArray = (value) => {
  return value.constructor.toString().indexOf("Array") > -1;
}

module.exports = {
  isArray,
}
